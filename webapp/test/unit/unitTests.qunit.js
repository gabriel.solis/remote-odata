/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"Tutoriales/Walkthrough-26-Remote-OData/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});