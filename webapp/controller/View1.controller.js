sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("Tutoriales.Walkthrough-26-Remote-OData.controller.View1", {
		onInit: function () {
			var oDataS = this.getView().getModel("");
			console.log(oDataS);
		},
		formatDate: function (dBirthday) {
			 var oFormat = sap.ui.core.format.DateFormat.getInstance({
				format: "yMMMd"
			});
			return oFormat.format(dBirthday);
		}
	});
});